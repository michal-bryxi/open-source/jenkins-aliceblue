let form = document.getElementById("form");

const MODULES = [
  {
    id: 'jenkins_pipeline_log_auto_collapse',
    label: 'Auto collapse pipeline logs'
  },
  {
    id: 'jenkins_pipeline_status_to_favicon',
    label: 'Show pipeline status as favicon'
  }
];

function handleCheckboxClick(event) {
  const payload = {[event.target.name]: event.target.checked}
  chrome.storage.sync.set({[event.target.name]: event.target.checked});
}

function constructOptions(modules) {
  for (let module of modules) {
    chrome.storage.sync.get(module.id, (data) => {
      let label = document.createElement('label');
      label.classList.add('inline-flex', 'items-center');
      
      let checkbox = document.createElement('input');
      checkbox.classList.add('mr-2');
      checkbox.setAttribute('type', 'checkbox');
      checkbox.setAttribute('name', module.id);
      checkbox.checked = data[module.id] ?? false;
      checkbox.addEventListener("click", handleCheckboxClick);
      
      label.appendChild(checkbox)
      label.appendChild(document.createTextNode(module.label))

      form.appendChild(label);
    });
  }
}

constructOptions(MODULES);
