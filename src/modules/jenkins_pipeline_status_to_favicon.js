(function() {

  const SETTINGS_KEY = 'jenkins_pipeline_status_to_favicon';

  chrome.storage.sync.get(SETTINGS_KEY, (data) => {
    if(data[SETTINGS_KEY]) {
      const ICON_SELECTOR = '.Header-topNav-inner title';
      const LINK_ELEMENT_ID = 'pipeline-status-favicon';

      function pipelineStatusToFavicon() {
          let linkEl = document.head.querySelector(`#${LINK_ELEMENT_ID}`);

          if (!linkEl) {
            linkEl = document.createElement('link');
            linkEl.id = LINK_ELEMENT_ID;
            linkEl.rel = 'icon';
            document.head.appendChild(linkEl);
          }

          const statusTitle = document.querySelector(ICON_SELECTOR).textContent;

          const faviconSvg = document.createElement('svg');

          faviconSvg.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

          const faviconTextEl = document.createElement('text');

          faviconTextEl.setAttribute('font-size', '32');
          faviconTextEl.setAttribute('y', '28');

          const icons = {
            aborted: '⚪️',
            success: '🟢',
            failure: '🔴',
            running: '🔵',
          };

          faviconTextEl.textContent = icons[statusTitle] || '❓';

          faviconSvg.appendChild(faviconTextEl);
          linkEl.href = `data:image/svg+xml,${faviconSvg.outerHTML}`;
      }

      const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
          for (const added of mutation.addedNodes) {
            if (added.nodeType === Node.ELEMENT_NODE && added.querySelector(ICON_SELECTOR)) {
              pipelineStatusToFavicon();
            }
          }
          if (mutation.target.nodeType === Node.ELEMENT_NODE && mutation.target.querySelector(ICON_SELECTOR)) {
            pipelineStatusToFavicon();
          }
        });
      });

      const config = {
        subtree: true,
        attributes: false,
        childList: true,
        characterData: true,
      };

      observer.observe(document.body, config);
    }
  });

}());