(function() {

  const SETTINGS_KEY = 'jenkins_pipeline_log_auto_collapse';

  chrome.storage.sync.get(SETTINGS_KEY, (data) => {
    if(data[SETTINGS_KEY]) {
      const LOG_HEAD_SELECTOR = '.logConsole .result-item.failure.expanded .result-item-head';
        
      const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
          for (const added of mutation.addedNodes) {
            if (added.nodeType === Node.ELEMENT_NODE && added.querySelector(LOG_HEAD_SELECTOR)) {
              added.querySelector(LOG_HEAD_SELECTOR).click();
            }
          }
        });
      });
      
      const config = {
        subtree: true,
        attributes: false,
        childList: true,
        characterData: true,
      };
      
      observer.observe(document.body, config);
    }
  });
  
}());