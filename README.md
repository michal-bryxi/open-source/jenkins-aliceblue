## Jenkins AliceBlue

Chrome extension to improve Jenkins Blue experience.

### Modules

#### Jenkins pipeline status to favicon

Changes favicon of currently open browser tab to match pipeline status.
According to following key:

```
aborted: '⚪️',
success: '🟢',
failure: '🔴',
running: '🔵',
```
### Jenkins pipeline log auto collapse

For some reason Jenkins automatically expands last failing part  of the log.
This causes the UI to auto-scroll to the bottom of said log.
This is often times not desired as the logs themselves are too verbose to be useful.
This module makes sure that said section is collapsed, thus preventing said auto-scroll.

### Installation

- Chrome Web Store publication is pending ⏳

#### Manual installation

1. Install [volta](https://volta.sh/)
2. Build extension:
```sh
yarn install
yarn build
```
3. [Load unpacked](https://developer.chrome.com/docs/extensions/mv3/getstarted/#unpacked) extension to Chrome